﻿using System;
using System.Web.Mvc;
using Domain;
using SwissEphemerisWrapper;

namespace Anastasia.Controllers
{
    public class NatalChartController : Controller
    {
        private SwissEphemerisFacade swissEphemerisFacade;

        public NatalChartController()
        {
            swissEphemerisFacade = new SwissEphemerisFacade();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetHoroscope(HoroscopeInput horoscopeInput)
        {
            var allPlanetaryValues = swissEphemerisFacade.GetAllPlanetaryValues(horoscopeInput);

            return View("BirthChart",allPlanetaryValues);
        }
    }
}
