﻿using System;

namespace Anastasia.Utils
{
    public static class AngleUtils
    {
        public static string GetDegreesMinutesSeconds(double decimalDegrees)
        {
            double degrees = Math.Floor(decimalDegrees);
            double minutes = (decimalDegrees - Math.Floor(decimalDegrees)) * 60.0;
            double seconds = (minutes - Math.Floor(minutes)) * 60.0;
            return degrees + ":" + Math.Floor(minutes) + ":" + Math.Round(seconds);
        }
    }
}