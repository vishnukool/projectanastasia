﻿namespace Domain
{
    public enum Graha
    {
        ASCENDANT,
        SUN,
        MOON,
        MERCURY,
        VENUS,
        MARS,
        JUPITER,
        SATURN,
        RAHU,
        KETU
    }
}