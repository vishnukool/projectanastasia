﻿using System;

namespace Domain
{
    public class Planet
    {
        private double degree;

        public double Degree
        {
            get { return degree; }
            set
            {
                if (degree > 30) throw new Exception("House degrees greater than 30, wtf are you doing !");
                degree = value;
            }
        }

        public Rashi Rashi { get; set; }

        public Graha Graha { get; set; }
    }
}