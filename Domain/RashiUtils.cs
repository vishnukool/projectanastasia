﻿using System;

namespace Domain
{
    public class RashiUtils
    {
        public Rashi GetRashiFromTotalLongitude(double longitude)
        {
            var rashiNumber = (int) (longitude/30);

            switch (rashiNumber)
            {
                case 0:
                    return Rashi.Mesha;
                case 1:
                    return Rashi.Vrishabha;
                case 2:
                    return Rashi.Mithuna;
                case 3:
                    return Rashi.Karkataka;
                case 4:
                    return Rashi.Simha;
                case 5:
                    return Rashi.Kanya;
                case 6:
                    return Rashi.Thula;
                case 7:
                    return Rashi.Vrischika;
                case 8:
                    return Rashi.Dhanu;
                case 9:
                    return Rashi.Makara;
                case 10:
                    return Rashi.Kumbha;
                case 11:
                    return Rashi.Meena;
            }
            throw new Exception("Planetary Longitude is  beyond 360 !! wtf are you doing");
        }
    }
}