﻿namespace Domain
{
    public enum Rashi
    {
        Mesha,
        Vrishabha,
        Mithuna,
        Karkataka,
        Simha,
        Kanya,
        Thula,
        Vrischika,
        Dhanu,
        Makara,
        Kumbha,
        Meena
    }
}