﻿using System;

namespace Domain
{
    public class HoroscopeInput
    {
        public DateTime BirthDateTime { get; set; }
        
        public double Latitude { get; set; }
        
        public double Longitude { get; set; }

        public Ayanamsha Ayanamsha { get; set; }
    }

    public enum Ayanamsha
    {
        Lahiri,
        KPOld,
        KPNew
    }
}