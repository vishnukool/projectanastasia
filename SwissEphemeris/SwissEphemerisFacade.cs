﻿
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Domain;
using NUnit.Framework;

namespace SwissEphemerisWrapper
{
    public class SwissEphemerisFacade
    {

        public SwissEphemerisFacade()
        {
            rashiUtils = new RashiUtils();
        }

        private const string IndiaStandardTimeZoneId = "India Standard Time";
        private readonly int computationFlag = EphemerisModes.Sidereal | EphemerisModes.SwissEphemeris
                                                                            | EphemerisModes.Topocentric;

        private RashiUtils rashiUtils;

        [DllImport(@"../../lib/swedll32.dll")]
        private static extern void swe_close();

        [DllImport(@"../../lib/swedll32.dll")]
        static extern void swe_set_ephe_path([MarshalAs(UnmanagedType.LPTStr)] StringBuilder path);

        [DllImport(@"../../lib/swedll32.dll", CharSet = CharSet.Ansi)]
        private static extern int swe_calc_ut(double julianDayNumber, int planetNumber, Int32 computationFlag, double[] longitudeAndLatitude, StringBuilder errorMessage);
        [DllImport(@"../../lib/swedll32.dll", CharSet = CharSet.Ansi)]
        private static extern int swe_set_sid_mode(Int32 siderealMode, double referenceDate, double initialAyanamsha);

        [DllImport(@"../../lib/swedll32.dll", CharSet = CharSet.Ansi)]
        private static extern int swe_houses_ex(double julianDayNumber, int sideralAndRadiansFlag, double latitude, double longitude,
            int houseMethodAsciiCode, double[] cusps, double[] ascendantAndSomeOtherCrap);

        [DllImport(@"../../lib/swedll32.dll", CharSet = CharSet.Ansi)]
        private static extern double swe_sidtime(double julianDayNumber);

        [DllImport(@"../../lib/swedll32.dll")]
        private static extern int swe_utc_to_jd(int year, int month, int day, int hour, int minute, double second, int gregflag,
            double[] julianDayNumbersInEtAndUt, StringBuilder errorMessage);

        [Test]
        public void ShouldGetAllPlanetValues()
        {
            var errorMessage = new StringBuilder(1000);
            var longitudeAndLatitude = new Double[6];
            var cusps = new Double[13];
            var ascentantAndMore = new Double[10];
            var julianDayNumbersInEtAndUt = new double[2];
            TimeZoneInfo indiaTimeZone = TimeZoneInfo.FindSystemTimeZoneById(IndiaStandardTimeZoneId);
            var ephemerisTablesPath = new StringBuilder(@"../../../EphemerisFiles/");

            swe_set_ephe_path(ephemerisTablesPath);
            var birthTimeInIndianZone = new DateTime(2010, 2, 3, 20, 30, 0);
            DateTime birthTimeInUtc = TimeZoneInfo.ConvertTimeToUtc(birthTimeInIndianZone, indiaTimeZone);

            int errorCOde = swe_utc_to_jd(birthTimeInUtc.Year, birthTimeInUtc.Month, birthTimeInUtc.Day,
                                          birthTimeInUtc.Hour, birthTimeInUtc.Minute, birthTimeInUtc.Second, 1,
                                          julianDayNumbersInEtAndUt, errorMessage);
            if (errorCOde != 0)
            {
                Assert.Fail("Error ho gaya julian date conversion par !!");
            }
            Console.WriteLine("Julian Day Number from UTC: " + julianDayNumbersInEtAndUt[1]);
            Console.WriteLine(swe_sidtime(julianDayNumbersInEtAndUt[1]));
            var computationFlag = EphemerisModes.Sidereal | EphemerisModes.SwissEphemeris;
            //                                                                            | EphemerisModes.Topocentric;
            //            swe_set_topo(12.9667, 77.5667, 0);
            //            swe_set_topo(12.9667, 77.5667, 0);
            swe_set_sid_mode(SiderealFlag.Lahiri, 0, 0);
            foreach (var planet in Planets.AllPlanets)
            {
                int sweCalcUt = swe_calc_ut(julianDayNumbersInEtAndUt[1], planet, computationFlag, longitudeAndLatitude, errorMessage);
                Console.WriteLine("Planet Number " + planet + ": " + GetDegreesMinutesSeconds(longitudeAndLatitude[0]));
            }
            swe_houses_ex(julianDayNumbersInEtAndUt[1], EphemerisModes.Sidereal, 12.9667, 77.5667, 'A', cusps, ascentantAndMore);
            Console.WriteLine("Ascendant: " + GetDegreesMinutesSeconds(ascentantAndMore[0]));
        }

        public IList<Planet> GetAllPlanetaryValues(HoroscopeInput horoscopeInput)
        {
            var birthDateTime = horoscopeInput.BirthDateTime;
            var errorMessage = new StringBuilder(1000);
            var longitudeAndLatitude = new Double[6];
            var cusps = new Double[13];
            var ascentantAndMore = new Double[10];
            var julianDayNumbersInEtAndUt = new double[2];
            var indiaTimeZone = TimeZoneInfo.FindSystemTimeZoneById(IndiaStandardTimeZoneId);
            var ephemerisTablesPath = new StringBuilder(@"../../../EphemerisFiles/");
            var houses = new List<Planet>();

            swe_set_ephe_path(ephemerisTablesPath);
            DateTime birthTimeInUtc = TimeZoneInfo.ConvertTimeToUtc(birthDateTime, indiaTimeZone);

            int errorCode = swe_utc_to_jd(birthTimeInUtc.Year, birthTimeInUtc.Month, birthTimeInUtc.Day,
                                          birthTimeInUtc.Hour, birthTimeInUtc.Minute, birthTimeInUtc.Second, 1,
                                          julianDayNumbersInEtAndUt, errorMessage);
            if (errorCode != 0)
            {
                throw new Exception("Error ho gaya julian date conversion par !!");
            }
            var computationFlag = EphemerisModes.Sidereal | EphemerisModes.MoshierEphemeris;
            swe_set_sid_mode(SiderealFlag.Lahiri, 0, 0);
            foreach (var planet in Planets.AllPlanets)
            {
                int sweCalcUt = swe_calc_ut(julianDayNumbersInEtAndUt[1], planet, computationFlag, longitudeAndLatitude, errorMessage);
                if (!string.IsNullOrEmpty(errorMessage.ToString()))
                {
                    throw new Exception(errorMessage.ToString());
                }
                houses.Add(GetPlanetForLongitude(planet, longitudeAndLatitude[0]));
                if (planet == Planets.MEAN_NODE)
                {
                    //add ketu during rahu's turn 
                    houses.Add(GetPlanetForLongitude(Planets.KETU, GetKetuLongitudeFromRahu(longitudeAndLatitude[0])));        
                }
            }
            swe_houses_ex(julianDayNumbersInEtAndUt[1], EphemerisModes.Sidereal, 12.9667, 77.5667, 'A', cusps, ascentantAndMore);
            houses.Add(GetPlanetForLongitude(Planets.ASCENDANT, ascentantAndMore[0]));
            return houses;
        }

        private double GetKetuLongitudeFromRahu(double longitude)
        {
            return (longitude + 180)%360;
        }

        private Planet GetPlanetForLongitude(int planetNumber, double longitude)
        {
            var planet = new Planet
                {
                    Degree = longitude%30,
                    Rashi = rashiUtils.GetRashiFromTotalLongitude(longitude)
                };

            if (planetNumber == Planets.SUN) planet.Graha = Graha.SUN;
            else if (planetNumber == Planets.MOON) planet.Graha = Graha.MOON;
            else if (planetNumber == Planets.MERCURY) planet.Graha = Graha.MERCURY;
            else if (planetNumber == Planets.VENUS) planet.Graha = Graha.VENUS;
            else if (planetNumber == Planets.MARS) planet.Graha = Graha.MARS;
            else if (planetNumber == Planets.JUPITER) planet.Graha = Graha.JUPITER;
            else if (planetNumber == Planets.SATURN) planet.Graha = Graha.SATURN;
            else if (planetNumber == Planets.MEAN_NODE) planet.Graha = Graha.RAHU;
            else if (planetNumber == Planets.KETU) planet.Graha = Graha.KETU;
            else if (planetNumber == Planets.ASCENDANT) planet.Graha = Graha.ASCENDANT;
            else throw new Exception("Planet Number being evaluated is none of the 9 vedic astrology standard ones ");

            return planet;
        }


        public double ConvertDegreeAngleToDouble(double degrees, double minutes, double seconds)
        {
            return degrees + (minutes / 60) + (seconds / 3600);
        }

        public string GetDegreesMinutesSeconds(double decimalDegrees)
        {
            double degrees = Math.Floor(decimalDegrees);
            double minutes = (decimalDegrees - Math.Floor(decimalDegrees)) * 60.0;
            double seconds = (minutes - Math.Floor(minutes)) * 60.0;
            return degrees + ":" + Math.Floor(minutes) + ":" + Math.Round(seconds);
        }

    }
}
